package reputation.node.enums;

/**
 * Enumerador com os tipos possíveis de serviços que um nó pode prestar.
 * 
 * @author Allan Capistrano
 * @version 1.0.0
 */
public enum NodeServiceType {
  RND_DEVICE_ID
}
